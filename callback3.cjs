const cardsData = require("./cards_1.json");

function getCardsByListID(listID) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const cards = cardsData[listID]; 

            if (cards && Array.isArray(cards)) {
                resolve(cards);
            } else {
                reject("No cards found ");
            }
        }, 2000);
    });
}

module.exports = getCardsByListID;
