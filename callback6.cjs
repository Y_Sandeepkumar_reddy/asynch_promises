const getBoard = require("./callback1.cjs");
const boardlist = require("./callback2.cjs");
const getCardsByListID = require("./callback3.cjs");

function thanosInfo() {
    return getBoard("mcu453ed")
        .then(board => {
            console.log("Information from the Thanos board:");
            console.log(board);
            // Pass the board to the next .then() block
            return boardlist(board.id)
                .then(lists => {
                    console.log("Lists found for boardID:", board.id);
                    console.log("Lists:", lists);
                    // Map each list to a promise to fetch cards
                    const cardsPromises = lists.map(list => {
                        return getCardsByListID(list.id)
                            .then(cards => {
                                console.log(`All cards for list ${list.name} (${list.id}):`);
                                console.log(cards);
                                return cards;
                            })
                            .catch(error => {
                                console.log(`Error fetching cards for list ${list.id}:`, error);
                                return [];
                            });
                    });
                    // Resolve with all cards fetched successfully
                    return Promise.all(cardsPromises);
                });
        })
        .then(() => {
            return "All cards fetched successfully.";
        })
        .catch(error => {
            console.log("Error in fetching cards:", error);
            return Promise.reject(`Error in fetching cards: ${error}`);
        });
}

module.exports = thanosInfo;