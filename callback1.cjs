const boards = require("./boards_1.json");

function getBoard(boardID) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const board = boards.find(board => board.id === boardID);

            if (board) {
                resolve(board);
            } else {
                reject("Board not found");
            }
        }, 2000);
    });
}

module.exports = getBoard;