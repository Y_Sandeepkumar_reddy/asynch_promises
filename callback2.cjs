const lists = require("./lists_1.json");

function getBoardLists(boardID) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const boardLists = lists[boardID];

            if (boardLists) {
                console.log("Lists found for boardID:", boardID);
                resolve(boardLists);
            } else {
                reject("No lists found for the specified board ID");
            }
        }, 2000);
    });
}

module.exports = getBoardLists;
