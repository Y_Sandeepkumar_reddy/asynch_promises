const getBoard = require("./callback1.cjs");
const boardlist = require("./callback2.cjs");
const getCardsByListID = require("./callback3.cjs");

function thanosInfo() {
    // Step 1: Get information from the Thanos board
    getBoard("mcu453ed")
        .then(board => {
            console.log("Information from the Thanos board:");
            console.log(board);

            // Step 2: Get all the lists for the Thanos board
            return boardlist(board.id);
        })
        .then(lists => {
            console.log("All lists for the Thanos board:");
            console.log(lists);

            // Step 3: Get all cards for the Mind and Space lists
            const mindList = lists.find(list => list.name === "Mind");
            const spaceList = lists.find(list => list.name === "Space");

            if (!mindList || !spaceList) {
                throw new Error("Mind or Space list not found.");
            }

            // Fetch cards for the Mind list
            return getCardsByListID(mindList.id)
                .then(mindCards => {
                    console.log("All cards for the Mind list:");
                    console.log(mindCards);
                    // Return space list ID to fetch space cards
                    return spaceList.id;
                });
        })
        .then(spaceListId => {
            // Fetch cards for the Space list
            return getCardsByListID(spaceListId)
                .then(spaceCards => {
                    console.log("All cards for the Space list:");
                    console.log(spaceCards);
                });
        })
        .catch(error => {
            console.error(error);
        });
}

module.exports = thanosInfo;
