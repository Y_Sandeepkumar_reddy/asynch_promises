const getCardsByListID = require("../callback3.cjs");

getCardsByListID("ghnb768")
    .then(cards => {
        console.log("Cards belonging to the list:", cards);
    })
    .catch(error => {
        console.error("Error:", error);
    });
