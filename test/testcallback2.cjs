const getBoardLists = require("../callback2.cjs");

getBoardLists("mcu453ed")
    .then(lists => {
        console.log("Lists belonging to the board:", lists);
    })
    .catch(error => {
        console.error(error);
    });
