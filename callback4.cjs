const getBoard = require("./callback1.cjs");
const boardlist = require("./callback2.cjs");
const getCardsByListID = require("./callback3.cjs");

function thanosInfo() {
    // Step 1: Get information from the Thanos board
    getBoard("mcu453ed")
        .then(board => {
            console.log("Information from the Thanos board:");
            console.log(board);

            // Step 2: Get all the lists for the Thanos board
            return boardlist(board.id);
        })
        .then(lists => {
            console.log("All lists for the Thanos board:");
            console.log(lists);

            // Step 3: Get all cards for the Mind list simultaneously
            const mindList = lists.find(list => list.name === "Mind");
            return getCardsByListID(mindList.id);
        })
        .then(mindCards => {
            console.log("All cards for the Mind list:");
            console.log(mindCards);
        })
        .catch(error => {
            console.error(error);
        });
}

module.exports = thanosInfo;
